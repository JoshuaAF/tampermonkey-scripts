// ==UserScript==
// @name         Mastodon column width slider
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Pretty much what you'd expect
// @author       Joshua Fuller
// @match        *://mastodon.technology/*
// @grant        none
// ==/UserScript==

(function() {
    var body = document.getElementsByTagName('body')[0];
    var slider = document.createElement('input');

    slider.type = 'range';
    slider.style.cssText = `
      position: fixed;
      z-index: 100;
      right: 5px;
      top: 5px;

      width: 80px;
      height: 20px;
      opacity: 0.1;
    `;

    slider.value = 330;
    slider.min = 300;
    slider.max = 800;

    slider.onchange = slider.oninput = function(){
      let columns = document.getElementsByClassName('column');
      for(var i = 0; i < columns.length; i++) {
        columns[i].style.width = slider.value + 'px';
      }
    };

    slider.onmouseenter = function() {
      slider.style.opacity = 0.6;
    };

    slider.onmouseexit = function() {
      slider.style.opacity = 0.1;
    }

    body.appendChild(slider);
})();